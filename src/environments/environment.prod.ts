export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyAFwZ5EdQJWOnXz_qmf3xodl-qDrIXDpyk',
    authDomain: 'bib-exercice.firebaseapp.com',
    databaseURL: 'https://bib-exercice.firebaseio.com',
    projectId: 'bib-exercice',
    storageBucket: 'bib-exercice.appspot.com',
    messagingSenderId: '408168960348'
  }
};
export const config = {
  users: 'users',
  books: 'bibliotheque',
  userRole: {
    lecteur: 'lecteur',
    bib: 'bibliothecaire'
  }
}
