// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: true,
  firebase: {
    apiKey: 'AIzaSyAFwZ5EdQJWOnXz_qmf3xodl-qDrIXDpyk',
    authDomain: 'bib-exercice.firebaseapp.com',
    databaseURL: 'https://bib-exercice.firebaseio.com',
    projectId: 'bib-exercice',
    storageBucket: 'bib-exercice.appspot.com',
    messagingSenderId: '408168960348'
  }
};
export const config = {
  users: 'users',
  books: 'bibliotheque',
  userRole: {
    lecteur: 'lecteur',
    bib: 'bibliothecaire'
  }
}
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
