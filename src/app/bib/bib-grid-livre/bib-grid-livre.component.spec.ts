import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BibGridLivreComponent } from './bib-grid-livre.component';

describe('BibGridLivreComponent', () => {
  let component: BibGridLivreComponent;
  let fixture: ComponentFixture<BibGridLivreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BibGridLivreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BibGridLivreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
