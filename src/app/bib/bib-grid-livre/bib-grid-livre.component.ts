import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Livre, LivreId} from '../../data/data';
import {AngularFirestore} from '@angular/fire/firestore';
import {EMPTY_MAP} from '@angular/core/src/view';

@Component({
  selector: 'app-bib-grid-livre',
  templateUrl: './bib-grid-livre.component.html',
  styleUrls: ['./bib-grid-livre.component.css']
})
export class BibGridLivreComponent implements OnInit {
  @Input() booksId: LivreId[];
  @Output() bookModify: EventEmitter<LivreId> = new EventEmitter<LivreId>();
  @Output() bookDelete: EventEmitter<LivreId> = new EventEmitter<LivreId>();
  constructor(private db: AngularFirestore) {
  }
  ngOnInit() {
  }

  modifiedBook($event: LivreId) {
    this.bookModify.emit($event);
  }

  deleteBook($event: LivreId) {
    this.bookDelete.emit($event);
  }
}
