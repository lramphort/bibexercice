import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BibAccueilComponent } from './bib-accueil.component';

describe('BibAccueilComponent', () => {
  let component: BibAccueilComponent;
  let fixture: ComponentFixture<BibAccueilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BibAccueilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BibAccueilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
