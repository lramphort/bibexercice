import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AngularFireAuth} from '@angular/fire/auth';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularFirestore} from '@angular/fire/firestore';
import {Livre, LivreId, User} from '../../data/data';
import {config} from '../../../environments/environment';

@Component({
  selector: 'app-bib-accueil',
  templateUrl: './bib-accueil.component.html',
  styleUrls: ['./bib-accueil.component.css']
})
export class BibAccueilComponent implements OnInit {
  public books: Livre[];
  public booksId: LivreId[];
  public showAdd: boolean ;
  public bookForm: FormGroup;
  public user: User;
  constructor(private router: Router, private AFAuth: AngularFireAuth, private db: AngularFirestore, fb: FormBuilder) {
    db.collection<Livre>(config.books).valueChanges().subscribe((books: Livre[]) => {
      this.books = books;
    });
    this.db.collection(config.books)
      .snapshotChanges()
      .subscribe((snapShot) => {
          this.books = snapShot
            .map((element) => element.payload.doc.data() as Livre);
          this.booksId = snapShot
            .map((element) => new LivreId(element.payload.doc.id, element.payload.doc.data() as Livre));
        }
      );
    this.bookForm = fb.group({
      'titre': ['', Validators.compose([Validators.required])],
      'auteur': ['', Validators.compose([Validators.required])],
      'annee': ['', Validators.compose([Validators.required])],
    });
  }

  ngOnInit() {
    this.showAdd = false;

    this.AFAuth.user.subscribe( (currentUser) => {
      this.db.collection<User>(config.users)
        .doc(currentUser.uid)
        .valueChanges()
        .subscribe((user: User) => {
            this.user = user;
          }
        );
    });
  }
  add() {
    this.db.collection<Livre>(config.books).add({
      titre: this.bookForm.controls['titre'].value,
      auteur: this.bookForm.controls['auteur'].value,
      annee: this.bookForm.controls['annee'].value,
      emprunte: ''
    });
    this.clear();
    this.showAdd = false;
  }
  clear() {
    this.bookForm.reset();
  }

  keyPressed($event: KeyboardEvent) {
    if ( $event.key === 'Enter' ) {
      this.add();
    }
  }

  unconnect() {
    this.AFAuth.auth.signOut();
    this.router.navigate(['connect']);
  }

  bookToModify($event: LivreId) {
    this.db.collection<Livre>(config.books).doc($event.id).update($event.livre);
  }

  bookToDelete($event: LivreId) {
    this.db.collection<Livre>(config.books).doc($event.id).delete();
  }
}
