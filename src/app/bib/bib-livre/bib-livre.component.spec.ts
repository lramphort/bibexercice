import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BibLivreComponent } from './bib-livre.component';

describe('BibLivreComponent', () => {
  let component: BibLivreComponent;
  let fixture: ComponentFixture<BibLivreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BibLivreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BibLivreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
