import {Component} from '@angular/core';
import {MatDialogRef} from '@angular/material';

@Component({
  selector: 'app-delete-dialog-overview',
  templateUrl: './deleteDialog.html',
})
export class DeleteDialogComponent {
  constructor( public dialogRef: MatDialogRef<DeleteDialogComponent>) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
}
