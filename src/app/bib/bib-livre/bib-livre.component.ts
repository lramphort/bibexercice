import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Livre, LivreId} from '../../data/data';
import {FormsModule} from '@angular/forms';
import {MatDialog} from '@angular/material';
import {DeleteDialogComponent} from './deleteDialog';

@Component({
  selector: 'app-bib-livre',
  templateUrl: './bib-livre.component.html',
  styleUrls: ['./bib-livre.component.css']
})
export class BibLivreComponent implements OnInit {
  @Input('livre') livreId: LivreId;
  @Output() livreModifie: EventEmitter<LivreId> = new EventEmitter<LivreId>();
  @Output() livreSupprime: EventEmitter<LivreId> = new EventEmitter<LivreId>();
  modify: boolean;
  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
    this.modify = false;
  }

  modifyItem($event: boolean) {
    if ($event) {
      this.livreModifie.emit(this.livreId);
    }
  }

  deleteItem() {
    if (this.livreId.livre.emprunte === '') {
      this.livreSupprime.emit(this.livreId);
    } else {
      this.openDialog();
    }
  }
  openDialog(): void {
    this.dialog.open(DeleteDialogComponent);
  }
}
