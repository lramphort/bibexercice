export class Livre {
  annee: number;
  auteur: string;
  emprunte: string; // contains the name of the person who borrows the book
  titre: string;
}

export class LivreId {
  constructor( id: string, livre: Livre ) {
    this.id = id;
    this.livre = livre;
  }
  id: string;
  livre: Livre;
}

export class User {
  role: string;
  nom: string;
  prenom: string;
}
