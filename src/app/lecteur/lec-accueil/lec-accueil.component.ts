import { Component, OnInit } from '@angular/core';
import {Livre, LivreId, User} from '../../data/data';
import {config} from '../../../environments/environment';
import {AngularFirestore} from '@angular/fire/firestore';
import {AngularFireAuth} from '@angular/fire/auth';
import {Router} from '@angular/router';
import {transition} from '@angular/animations';

@Component({
  selector: 'app-lec-accueil',
  templateUrl: './lec-accueil.component.html',
  styleUrls: ['./lec-accueil.component.css']
})
export class LecAccueilComponent implements OnInit {
  public booksId: LivreId[];
  public borrowedBooksId: LivreId[];
  public books: Livre[];
  public borrowedBooks: Livre[];
  public user: User;
  public showUserContent: boolean;
  public showBooksToUser: boolean;
  constructor(private db: AngularFirestore, private AFAuth: AngularFireAuth, private router: Router) { }

  ngOnInit() {
    this.showUserContent = this.showBooksToUser = false;
    this.db.collection(config.books)
      .snapshotChanges()
      .subscribe((snapShot) => {
        this.booksId = snapShot
          .filter((element) => {
            return (element.payload.doc.data() as Livre).emprunte === '';
          })
          .map((element) => new LivreId(element.payload.doc.id, element.payload.doc.data() as Livre));
        this.borrowedBooksId = snapShot
          .filter((element) => {
            return (element.payload.doc.data() as Livre).emprunte === this.AFAuth.auth.currentUser.email;
          })
          .map((element) => new LivreId(element.payload.doc.id, element.payload.doc.data() as Livre));
        this.books = this.booksId.map((livre: LivreId) => livre.livre);
        this.borrowedBooks = this.borrowedBooksId.map((livre: LivreId) => livre.livre);
        this.showBooksToUser = true;
        }
       );
    this.AFAuth.user.subscribe( (currentUser) => {
      this.db.collection<User>(config.users)
        .doc(currentUser.uid)
        .valueChanges()
        .subscribe((user: User) => {
            this.user = user;
          this.showUserContent = true;
          }
        );
    });
  }

  unborrowBook(event: Livre) {
    const livre: LivreId = this.borrowedBooksId.find((livreId: LivreId) => livreId.livre === event);
    this.db.collection(config.books).doc(livre.id).update({emprunte: ''});
  }

  borrowBook(event: Livre) {
    const livre: LivreId = this.booksId.find((e: LivreId) => e.livre === event);
    this.db.collection(config.books).doc(livre.id).update({emprunte: this.AFAuth.auth.currentUser.email});
  }

  unconnect() {
    this.AFAuth.auth.signOut();
    this.router.navigate(['connect']);
  }
}
