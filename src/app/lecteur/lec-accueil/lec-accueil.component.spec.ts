import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LecAccueilComponent } from './lec-accueil.component';

describe('LecAccueilComponent', () => {
  let component: LecAccueilComponent;
  let fixture: ComponentFixture<LecAccueilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LecAccueilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LecAccueilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
