import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LecLivreComponent } from './lec-livre.component';

describe('LecLivreComponent', () => {
  let component: LecLivreComponent;
  let fixture: ComponentFixture<LecLivreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LecLivreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LecLivreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
