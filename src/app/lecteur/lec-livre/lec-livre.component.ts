import {Component, Input, OnInit} from '@angular/core';
import {Livre, LivreId} from '../../data/data';

@Component({
  selector: 'app-lec-livre',
  templateUrl: './lec-livre.component.html',
  styleUrls: ['./lec-livre.component.css']
})
export class LecLivreComponent implements OnInit {
  @Input() livre: Livre;
  constructor() { }

  ngOnInit() {
  }

}
