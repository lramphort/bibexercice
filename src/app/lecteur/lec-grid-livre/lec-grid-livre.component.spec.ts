import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LecGridLivreComponent } from './lec-grid-livre.component';

describe('LecGridLivreComponent', () => {
  let component: LecGridLivreComponent;
  let fixture: ComponentFixture<LecGridLivreComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LecGridLivreComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LecGridLivreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
