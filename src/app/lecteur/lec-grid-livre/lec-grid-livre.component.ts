import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Livre, LivreId} from '../../data/data';

@Component({
  selector: 'app-lec-grid-livre',
  templateUrl: './lec-grid-livre.component.html',
  styleUrls: ['./lec-grid-livre.component.css']
})
export class LecGridLivreComponent implements OnInit {
  @Input() books: Livre[];
  @Output() bookClickedEvent: EventEmitter<Livre> = new EventEmitter<Livre>();
  constructor() {
  }

  ngOnInit() {
  }

  bookClicked(item: Livre) {
    this.bookClickedEvent.emit(item);
  }
}
