import { Component, OnInit } from '@angular/core';
import {AngularFireAuth} from '@angular/fire/auth';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularFirestore} from '@angular/fire/firestore';
import {config} from '../../environments/environment';
import {User} from '../data/data';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {
  public signIn: boolean;
  public signInForm: FormGroup;
  public signInSuccess: boolean;
  public connexion: boolean;
  public connexionSuccess: boolean;
  public user: User;
  constructor(private AFAuth: AngularFireAuth,
              fb: FormBuilder,
              private db: AngularFirestore,
              private router: Router,
              private route: ActivatedRoute) {
    this.signInForm = fb.group({
      'email': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required])],
      'nom': ['', Validators.compose([Validators.required])],
      'prenom': ['', Validators.compose([Validators.required])],
      'type': ['', Validators.compose([Validators.required])]
    });
    this.signInForm = fb.group({
      'email': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required])]
    });
  }
  ngOnInit() {
    this.signIn = false;
    this.signInSuccess = false;
    this.connexion = false;
    this.connexionSuccess = false;
    this.AFAuth.auth.signOut()
      .then((res) => {
        console.log('signOut success');
      })
      .catch((err) => {
        console.log('no User');
      })
    ;
  }

  inscription(event: string) {
    console.log(event);
    if ( event !== '' ) {
      this.router.navigate([this.getRoute(event)]);
    } else {
      console.log('retourButton');
    }
    this.signIn = false;
  }

  connect(event: boolean) {
    if ( event ) {
      this.db.doc<User>(config.users + '/' + this.AFAuth.auth.currentUser.uid)
        .valueChanges().subscribe(data => {
          this.user = data;
          this.router.navigate([this.getRoute(this.user.role)]);
      });
    } else {
      console.log('return Button');
    }
    this.connexion = false;
  }

  private getRoute(s: string): string {
    if ( s === config.userRole.lecteur ) {
      return 'lecteur';
    } else if ( s === config.userRole.bib ) {
      return 'bib';
    } else {
      return 'connect';
    }
  }
}
