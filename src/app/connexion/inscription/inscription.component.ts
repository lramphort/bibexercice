import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularFireAuth} from '@angular/fire/auth';
import {AngularFirestore} from '@angular/fire/firestore';
import {config} from '../../../environments/environment';
import {User} from '../../data/data';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})

export class InscriptionComponent implements OnInit {
  public signInForm: FormGroup;
  @Output() retour: EventEmitter<string> = new EventEmitter<string>();
  public signInSuccess: boolean;
  public userRole = config.userRole;
  public validateCliked: boolean;
  public signInError: boolean;
  public addingUserOk: boolean;
  userEmailAlreadyUsed: boolean;
  constructor(fb: FormBuilder, private AFAuth: AngularFireAuth, private db: AngularFirestore) {
    this.signInForm = fb.group({
      'email': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required])],
      'passwordConfirm': ['', Validators.compose([Validators.required])],
      'nom': ['', Validators.compose([Validators.required])],
      'prenom': ['', Validators.compose([Validators.required])],
      'type': ['', Validators.compose([Validators.required])]
    }, {
      validator: this.matchPassword
    });
  }

  ngOnInit() {
    this.signInSuccess = false;
    this.validateCliked = false;
    this.signInError = false;
    this.addingUserOk = false;
    this.userEmailAlreadyUsed = false;
  }

  connexionMethod() {
    if (this.signInForm.valid) {
      this.validateCliked = true;
      this.AFAuth.auth.createUserWithEmailAndPassword(this.signInForm.controls['email'].value, this.signInForm.controls['password'].value)
        .then((res) => {
          this.signInSuccess = true;
          const userRole: User = {
            role: this.signInForm.controls['type'].value,
            nom: this.signInForm.controls['nom'].value.toString().toUpperCase(),
            prenom: this.signInForm.controls['prenom'].value
          };
          this.db.collection<User>(config.users).doc(res.user.uid).set(userRole)
            .then(() => {
              console.log('user created');
            })
            .catch((error) => {
              console.log(error);
            });
            this.quit(this.signInForm.controls['type'].value.toString());
          })
          .catch(
            (err) => {
              if (err.code === 'auth/email-already-in-use' ) {
                this.userEmailAlreadyUsed = true;
              }
              this.signInError = true;
              this.validateCliked = false;
            }
          );
    } else {
      this.signInError = true;
    }

  }

  quit(s: string) {
    this.retour.emit(s);
  }

  matchPassword( AC: AbstractControl) {
    const password: String = AC.get('password').value;
    const passwordConfirm: String = AC.get('passwordConfirm').value;
    if ( password !== passwordConfirm ) {
      AC.get('passwordConfirm').setErrors( {MatchPassword: true} );
    } else {
      return null;
    }
  }
}
