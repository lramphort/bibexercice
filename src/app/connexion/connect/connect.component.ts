import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {AngularFireAuth} from '@angular/fire/auth';

@Component({
  selector: 'app-connect',
  templateUrl: './connect.component.html',
  styleUrls: ['./connect.component.css']
})
export class ConnectComponent implements OnInit {
  connectForm: FormGroup;
  public logInSuccess: boolean;
  @Output() retour: EventEmitter<boolean> = new EventEmitter<boolean>();
  constructor(private AFAuth: AngularFireAuth, fb: FormBuilder) {
    this.connectForm = fb.group({
      'email': ['', Validators.compose([Validators.required])],
      'password': ['', Validators.compose([Validators.required])]
    });
  }

  ngOnInit() {
    this.logInSuccess = true;
  }
  connexion() {
    this.AFAuth.auth
      .signInWithEmailAndPassword(
        this.connectForm.controls['email'].value,
        this.connectForm.controls['password'].value)
      .then(() => {
        this.logInSuccess = true;
        this.retour.emit(this.logInSuccess);
        this.connectForm.reset();
      })
      .catch((err) => {
        console.log(err);
        this.logInSuccess = false;
      });
  }

  quit() {
    this.connectForm.reset();
    this.retour.emit(false);
  }

  onKeydown(event: KeyboardEvent) {
    if ( event.key === 'Enter' && this.connectForm.valid) {
      this.connexion();
    } else {
      console.log('button pressed');
    }
  }
}
