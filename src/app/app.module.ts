import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { environment } from '../environments/environment';
import { ConnexionComponent } from './connexion/connexion.component';
import { RootComponent } from './root/root.component';
import { BibGridLivreComponent } from './bib/bib-grid-livre/bib-grid-livre.component';
import { BibAccueilComponent } from './bib/bib-accueil/bib-accueil.component';
import { BibLivreComponent } from './bib/bib-livre/bib-livre.component';
import { LecAccueilComponent } from './lecteur/lec-accueil/lec-accueil.component';
import { LecGridLivreComponent } from './lecteur/lec-grid-livre/lec-grid-livre.component';
import { LecLivreComponent } from './lecteur/lec-livre/lec-livre.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatButtonModule, MatCardModule, MatDialog, MatDialogModule, MatIconModule, MatInputModule, MatRadioModule} from '@angular/material';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {ClickOutsideModule} from 'ng-click-outside';
import { InscriptionComponent } from './connexion/inscription/inscription.component';
import { ConnectComponent } from './connexion/connect/connect.component';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {RouterModule, Routes} from '@angular/router';
import {DeleteDialogComponent} from './bib/bib-livre/deleteDialog';

const route: Routes = [
  { path: '', redirectTo: '/connect', pathMatch: 'full' },
  { path: 'connect', component: ConnexionComponent},
  { path: 'lecteur', component: LecAccueilComponent},
  { path: 'bib', component: BibAccueilComponent }
]

@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase, 'bib-exercice'), // imports firebase/app needed for everything
    AngularFirestoreModule, // imports firebase/firestore, only needed for database features
    AngularFireAuthModule, // imports firebase/auth, only needed for auth features,
    AngularFireStorageModule, // imports firebase/storage only needed for storage features
    BrowserAnimationsModule,
    MatCardModule,
    MatButtonModule,
    MatIconModule,
    FormsModule,
    MatInputModule,
    ReactiveFormsModule,
    ClickOutsideModule,
    MatRadioModule,
    DragDropModule,
    MatDialogModule,
    RouterModule.forRoot(route)
  ],
  declarations: [
    AppComponent,
    ConnexionComponent,
    RootComponent,
    BibGridLivreComponent,
    BibAccueilComponent,
    BibLivreComponent,
    LecAccueilComponent,
    LecGridLivreComponent,
    LecLivreComponent,
    InscriptionComponent,
    ConnectComponent,
    DeleteDialogComponent
  ],
  bootstrap: [ AppComponent ],
  entryComponents: [
    DeleteDialogComponent
  ]
})
export class AppModule {}
