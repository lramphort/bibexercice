#Choix technologiques
##Back-end
J'ai choisis de développer cette application en Angular cli, 
car j'ai fréquemmenet urilisé cette technologie durant mes études.
J'ai choisis Firebase, un service de Google, pour le stockage des données, 
l'authentification des utilisateurs et la mise en ligne du site.
##Front-end
Pour le front-end, j'ai choisis Materials.

#Organisation du projet
Etapes durant l'implémentation ( dans l'ordre chronologique )
##Par écrit
- Représentation des données ( data/data.ts/Livre )
- Organisation grossière du site
  - page d'accueil pour lecteur
  - page d'accueil pour une bibliothécaire
  - page de connection
##Dans le code
- Connection avec firebase
- Lecteur 
  - Afficher des livres ( lecLivreComponent et lecGridLivreComponent )
  - page d'accueil d'un lecteur
  - page d'accueil d'une bibliothécaire
- Bibliothécaire 
  - page d'accueil
  - Affichier les livres avec le lecteur qui a emprunté le livre
  - Créer un livre
  - Modifier les informations d'un livre
 - Mise en place de routes
 - Corriger tous les bugs oubliés
 
 #Outils utlisés durant le développement
 - StackOverflow, développez.com, openclassroom pour les différentes erreurs rencontrées
 - La documentation fournie par Angular et Firebase
 - CodePen pour le front-end
 
